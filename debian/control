Source: xabacus
Section: games
Priority: optional
Maintainer: Jonathan Carter <jcc@debian.org>
Standards-Version: 4.6.1
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dpkg-dev (>= 1.16.1~),
               libaudiofile-dev,
               libaudio-dev,
               libmotif-dev,
               libxpm-dev,
               libxt-dev
Homepage: https://www.sillycycle.com/abacus.html
Vcs-Browser: https://salsa.debian.org/debian/xabacus
Vcs-Git: https://salsa.debian.org/debian/xabacus.git

Package: xabacus
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Provides: xabacus
Conflicts: xabacus
Replaces: xabacus
Description: simulation of the ancient calculator (plain X version)
 This program is an implementation of the original abacus, it provides
 the Chinese, Japanese, Korean, Roman and Russian version and can be
 modified to allow others.
 .
 This version was compiled without the Motif GUI widget library and thus
 shows limited user interface functionality. See xmabacus for the
 extended version.

Package: xmabacus
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Provides: xabacus
Conflicts: xabacus
Replaces: xabacus
Description: simulation of the ancient calculator (Motif version)
 This program is an implementation of the original abacus, it provides
 the Chinese, Japanese, Korean, Roman and Russian version and can be
 modified to allow others.
 .
 This is the Motif version which shows additional functionality. Motif
 is a GUI widget library for the X Window system.
