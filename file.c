/*
 * @(#)file.c
 *
 * Taken from xlock, many authors...
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#include "file.h"

char dirname[256];

static FILE *
carefulOpen(char *fileName, const char *type)
{
	FILE *fp = (FILE *) NULL;
	int s;
	struct stat fileStat;

	s = stat(fileName, &fileStat);
	if ((s >= 0 && S_ISREG(fileStat.st_mode)) || (s < 0 && type[0] == 'w')) {
		if ((fp = fopen(fileName, type)) == NULL)
			return (FILE *) NULL;
	} else {
		return (FILE *) NULL;
	}

	return fp;
}

static int
readable(char *fileName)
{
	FILE *fp;

	if ((fp = carefulOpen(fileName, "r")) == NULL)
		return 0;
	(void) fclose(fp);
	return 1;
}

static char *
baseName(char *fileName)
{
	int i;

	for (i = (int) strlen(fileName); i >= 0; i--) {
		if (fileName[i] == CHARDELIM
#ifdef WINVER
			|| fileName[i] == '/'
#endif
			)
		return &fileName[i + 1];
	}
	return fileName;
}

char *
findFile(char *fileName)
{
	if (readable(fileName))
		return fileName;
	else {
		char *temp = baseName(fileName);

#if 0
		(void) printf ("found file %s|%s\n", fileName, temp);
#endif
		if (readable(temp))
			return temp;
	}
	return NULL;
}

char *
getPWD()
{
#if 0
	int rc = GetCurrentDirectory(sizeof(dirname) - 1, dirname);

	dirname[rc] = '\0';
#else
	char *rc = getcwd(dirname, sizeof(dirname));

	if (rc == NULL)
		return NULL;
	else
#endif
	return dirname;
}

