#!/bin/sh
# @(#)Makefile.in
#
# Copyright 2017  David A. Bagley, bagleyd AT verizon.net
#
# All Rights Reserved
#
# Permission to use, copy, modify, and distribute this software and
# its documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and
# that both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of the author not be
# used in advertising or publicity pertaining to distribution of the
# software without specific, written prior permission.
#
# This program is distributed in the hope that it will be "useful",
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Simple audio script to play a sound file
##DEBUG
#echo $1
if test -x /usr/bin/aplay; then
  /usr/bin/aplay $1 2> /dev/null
elif test -x /usr/bin/paplay; then
  /usr/bin/paplay $1 2> /dev/null
elif test -x /usr/demo/SOUND/play; then
  # SUNOS 4.1.3 */
  /usr/demo/SOUND/play $1 2> /dev/null
elif test -x /usr/sbin/sfplay; then
  # IRIX 5.3
  /usr/sbin/sfplay $1 2> /dev/null
elif test -x /usr/bin/mme/decsound; then
  # Digital Unix with Multimedia Services
  /usr/bin/mme/decsound -play $1 2> /dev/null
elif test -c /dev/audio; then
  cat $1 > /dev/audio 2> /dev/null
elif test -c /dev/dsp; then
  cat $1 > /dev/dsp 2> /dev/null
elif test -c /dev/dsp1; then
  cat $1 > /dev/dsp1 2> /dev/null
elif test -c /dev/dsp2; then
  cat $1 > /dev/dsp2 2> /dev/null
fi
