# ABACUS TEACH
# Copyright (C) 2017 - 2022
# This file is distributed under the same license as the xabacus package.
# David Bagley <bagleyd AT verizon.net>, 2022.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: xabacus-8.4.1\n"
"Report-Msgid-Bugs-To: \n"
"Last-Translator: David Bagley <bagleyd AT verizon.net>\n"
"Language-Team: English <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#, c-format
msgid "Enter calculation X+Y, X-Y, X*Y, X/Y, Xv, or Xu where X and result nonnegative."
msgstr ""

msgid "Press enter to go through the calculation steps."
msgstr ""

msgid "add"
msgstr ""

msgid "Add"
msgstr ""

msgid "adding"
msgstr ""

msgid "adding digit"
msgstr ""

msgid "Adding"
msgstr ""

msgid "addition order"
msgstr ""

msgid "Almost done with position, need to add 1 to place working on"
msgstr ""

msgid "And clear primary"
msgstr ""

msgid "Answer"
msgstr ""

msgid "A-O field value"
msgstr ""

msgid "borrow"
msgstr ""

msgid "borrowing"
msgstr ""

msgid "Borrowing"
msgstr ""

msgid "carry"
msgstr ""

msgid "carrying"
msgstr ""

msgid "Carrying"
msgstr ""

msgid "Carrying in right A-O field"
msgstr ""

msgid "cube"
msgstr ""

msgid "Cube"
msgstr ""

msgid "Current answer"
msgstr ""

msgid "divide by"
msgstr ""

msgid "Dividing"
msgstr ""

msgid "Division overflow"
msgstr ""

msgid "Division underflow"
msgstr ""

msgid "Done"
msgstr ""

msgid "do nothing"
msgstr ""

msgid "Final answer"
msgstr ""

msgid "For rail"
msgstr ""

msgid "from lower deck"
msgstr ""

msgid "from primary field, offset digit by corresponding position"
msgstr ""

msgid "from primary to right auxiliary"
msgstr ""

msgid "from upper deck"
msgstr ""

msgid "grouping digits yields"
msgstr ""

msgid "in group"
msgstr ""

msgid "Left"
msgstr ""

msgid "left to right"
msgstr ""

msgid "Moving"
msgstr ""

msgid "multiplication order"
msgstr ""

msgid "Multiplication underflow"
msgstr ""

msgid "Multiplying"
msgstr ""

msgid "Now try a smaller value"
msgstr ""

msgid "on first auxiliary rail"
msgstr ""

msgid "on next move"
msgstr ""

msgid "on second auxiliary rail"
msgstr ""

msgid "onto lower deck"
msgstr ""

msgid "onto upper deck"
msgstr ""

msgid "part of"
msgstr ""

msgid "P-O field group value"
msgstr ""

msgid "put on"
msgstr ""

msgid "register"
msgstr ""

msgid "Right"
msgstr ""

msgid "right to left"
msgstr ""

msgid "root of"
msgstr ""

msgid "Root underflow"
msgstr ""

msgid "square"
msgstr ""

msgid "Square"
msgstr ""

msgid "Subtract"
msgstr ""

msgid "subtracting digit"
msgstr ""

msgid "Subtracting"
msgstr ""

msgid "Subtraction underflow"
msgstr ""

msgid "take off"
msgstr ""

msgid "Taking"
msgstr ""

msgid "to left A-O field, offset digit at position"
msgstr ""

msgid "to left A-O field, offset digit by corresponding group (or position)"
msgstr ""

msgid "to right A-O field, offset digit by corresponding position"
msgstr ""

msgid "Try a smaller value"
msgstr ""

msgid "with borrow"
msgstr ""

msgid "with carry"
msgstr ""

msgid "works best with lee option"
msgstr ""

msgid "Yes, OK to iterate"
msgstr ""
