$ save_verify='f$verify(0)
$! set ver
$!
$!      Compile and link for xabacus
$!
$! USAGE:
$! @make [debug clobber clean]
$!       debug : compile with debugger switch
$!       clean : clean all except executable
$!       clobber : clean all
$!
$! If you have
$!              XVMSUTILS library (VMS6.2 or lower)
$!              XPM library
$! insert the correct directory instead of X11:
$ xvmsutilsf="X11:XVMSUTILS.OLB"
$ xpmf="SYS$LIBRARY:LIBXPM.OLB"
$!
$! Already assumes DEC C on Alpha.
$! Assume VAX C on VAX.
$ decc=0
$! Assume DEC C on VAX.
$! decc=1
$!
$! if sound<>0 sound capability is included (only available on Alpha)
$! from vms_amd.c and vms_amd.h
$ sound=1
$! sound=0
$!
$ QUOTE = """""""""
$ QUOTE = QUOTE + QUOTE + QUOTE
$ data = "DATAPATH=" + QUOTE + "[]" + QUOTE
$ bump = "BUMPSOUND=" + QUOTE + "[]bump.au" + QUOTE
$ move = "MOVESOUND=" + QUOTE + "[]move.au" + QUOTE
$ drip = "DRIPSOUND=" + QUOTE + "[]drip.au" + QUOTE
$ defread=="''data',''bump',''move',''drip'"
$!
$! NOTHING SHOULD BE MODIFIED BELOW
$!
$ if p1 .eqs. "CLEAN" then goto Clean
$ if p1 .eqs. "CLOBBER" then goto Clobber
$!
$ axp=f$getsyi("HW_MODEL") .ge. 1024
$ sys_ver=f$edit(f$getsyi("version"),"compress")
$ if f$extract(0,1,sys_ver) .nes. "V"
$ then
$   type sys$input
This script will assume that the operating system version is at least V7.0.
$!
$   sys_ver="V7.0"
$ endif
$ sys_maj=0+f$extract(1,1,sys_ver)
$ if sys_maj .lt. 7
$ then
$   xvmsutils=f$search("''xvmsutilsf'") .nes. ""
$ endif
$ xpm=f$search("''xpmf'") .nes. ""
$ mmov=f$search("''mmovf'") .nes. ""
$!
$!
$! Create .opt file
$ close/nolog optf
$ open/write optf xabacus.opt
$!
$ defs=="VMS"
$ defs=="''defs',HAVE_MOTIF"
$ if xpm then defs=="''defs',HAVE_XPM"
$ if mmov .and. sound
$ then
$   defs=="''defs',HAS_MMOV"
$   if f$search("MMOV.DIR") .eqs. ""
$   then
$     create/dir [.mmov]
$     set def [.mmov]
$     copy SYS$COMMON:[SYSHLP.EXAMPLES.MMOV.COMMON]*.* []
$! spawn included to avoid unwanted redefinition of logicals
$     spawn @build_common
$     copy SYS$COMMON:[SYSHLP.EXAMPLES.MMOV.VIDEO]readavi.c []
$     spawn @[-]mmov
$     set def [-]
$   endif
$ endif
$! if have VMS port for libxml2, then add -DUSE_XML2
$ if axp .and. sound then defs=="''defs',USE_VMSPLAY"
$ defs=="''defread',''defs'"
$ defs=="''defs',LEE_ABACUS"
$ defs=="''defs',USE_SPIN"
$!
$!
$! Establish the Compiling Environment
$!
$! Set compiler command
$ if axp
$ then
$   cc=="cc/standard=vaxc/define=(''defs')"
$ else
$   if decc
$   then
$     cc=="cc/decc/standard=vaxc/define=(''defs')"
$   else ! VAX C
$     cc=="cc/define=(''defs')"
$   endif
$ endif
$ if p1 .eqs. "DEBUG" .or. p2 .eqs. "DEBUG" .or. p3 .eqs. "DEBUG"
$ then
$   if axp
$   then
$     cc=="cc/deb/noopt/standard=vaxc/define=(''defs')/list"
$   else
$     if decc
$     then
$       cc=="cc/deb/noopt/decc/standard=vaxc/define=(''defs')/list"
$     else ! VAX C
$       cc=="cc/deb/noopt/define=(''defs')/list"
$     endif
$   endif
$   link=="link/deb"
$ endif
$!
$ if axp .or. .not. decc
$ then
$   define/nolog sys sys$library
$ endif
$!
$ write sys$output "Compiling xabacus with ''defs'"
$ call make Abacus.obj  "cc Abacus.c"  Abacus.c AbacusP.h Abacus.h xwin.h file.h timer.h sound.h
$ call make AbacusU.obj  "cc AbacusU.c"  AbacusU.c AbacusP.h Abacus.h
$ call make AbacusC.obj  "cc AbacusC.c"  AbacusC.c AbacusP.h Abacus.h
$ call make AbacusD.obj  "cc AbacusD.c"  AbacusD.c AbacusP.h Abacus.h file.h
$ call make AbacusM.obj  "cc AbacusM.c"  AbacusM.c AbacusP.h Abacus.h
$ call make AbacusT.obj  "cc AbacusT.c"  AbacusT.c AbacusP.h Abacus.h
$ call make AbacusF.obj  "cc AbacusF.c"  AbacusF.c AbacusP.h Abacus.h
$ call make AbacusE.obj  "cc AbacusE.c"  AbacusE.c AbacusP.h Abacus.h
$ call make xwin.obj  "cc xwin.c"  xwin.c xwin.h
$ call make file.obj  "cc file.c"  file.c file.h
$ call make timer.obj  "cc timer.c"  timer.c timer.h
$ if axp .and. sound
$ then
$   call make sound.obj  "cc sound.c"  sound.c sound.h file.h vms_amd.h
$   call make vms_amd.obj  "cc vms_amd.c"  vms_amd.c vms_amd.h
$ else
$   call make sound.obj  "cc sound.c"  sound.c sound.h file.h
$ endif
$ call make xabacus.obj  "cc xabacus.c"  xabacus.c Abacus.h xwin.h file.h version.h
$!
$! Get libraries
$ if xpm then write optf "''xpmf'/lib"
$ if sys_maj .lt. 7
$ then
$   if xvmsutils then write optf "''xvmsutilsf'/lib"
$ endif
$! if .not. axp then write optf "sys$library:vaxcrtl/lib"
$ write optf "sys$library:vaxcrtl/lib"
$ if mmov
$ then
$   write optf "[.mmov]vms_mmov.obj"
$   write optf "[.mmov]readavi.obj"
$   write optf "[.mmov]commonlib.olb/lib"
$   write optf "sys$library:mmov.olb/lib"
$   write optf "sys$share:cma$open_rtl.exe/share"
$ endif
$ if axp then write optf "sys$library:ucx$ipc_shr/share"
$ if axp then write optf "sys$share:decw$xextlibshr/share"
$ if axp then write optf "sys$share:decw$xtlibshrr5/share"
$ if .not. axp then write optf "sys$library:ucx$ipc/lib"
$! write optf "sys$share:decw$dxmlibshr/share"
$ write optf "sys$share:decw$xmlibshr12/share"
$ write optf "sys$share:decw$xlibshr/share"
$ close optf
$!
$! LINK
$ write sys$output "Linking xabacus"
$ link/map xabacus/opt
$!
$ if mmov
$ then
$   write sys$output "NOTE:"
$   write sys$output "Sound files are played only when the user has the SYSNAM"
$   write sys$output "as an authorized privilege"
$ endif
$ set noverify
$ exit
$!
$Clobber:      ! Delete executables, Purge directory and clean up object files
$!                and listings
$ delete/noconfirm/log xabacus.exe;*
$!
$Clean:        ! Purge directory, clean up object files and listings
$ close/nolog optf
$ purge
$ delete/noconfirm/log *.lis;*
$ delete/noconfirm/log *.obj;*
$ delete/noconfirm/log *.opt;*
$ delete/noconfirm/log *.map;*
$!
$ exit
$!
! SUBROUTINE TO CHECK DEPENDENCIES
$ make: subroutine
$   v='f$verify(0)
$!   p1       What we are trying to make
$!   p2       Command to make it
$!   p3 - p8  What it depends on
$
$   if (f$extract(0,3,p2) .eqs. "cc ") then write optf "''p1'"
$   if (f$extract(0,3,p2) .eqs. "CC ") then write optf "''p1'"
$
$   if f$search(p1) .eqs. "" then goto MakeIt
$   time=f$cvtime(f$file(p1,"RDT"))
$   arg=3
$Loop:
$   argument=p'arg
$   if argument .eqs. "" then goto Exit
$   el=0
$Loop2:
$   file=f$element(el," ",argument)
$   if file .eqs. " " then goto Endl
$   afile=""
$Loop3:
$   ofile=afile
$   afile=f$search(file)
$   if afile .eqs. "" .or. afile .eqs. ofile then goto NextEl
$   if f$cvtime(f$file(afile,"RDT")) .gts. time then goto MakeIt
$   goto Loop3
$NextEL:
$   el=el+1
$   goto Loop2
$EndL:
$   arg=arg+1
$   if arg .le. 8 then goto Loop
$   goto Exit
$
$MakeIt:
$   set verify
$   'p2
$   vv='f$verify(0)
$Exit:
$   if v then set verify
$ endsubroutine
