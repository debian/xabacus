/*
 * @(#)file.h
 *
 * Taken from xlock, many authors...
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/* Header file for file */

#ifndef _file_h
#define _file_h

#ifdef WINVER
#ifdef UNIXDELIM
#define CURRENTDELIM "./"
#define FINALDELIM "/"
#define CHARDELIM '/'
#else
#define CURRENTDELIM ".\\"
#define FINALDELIM "\\"
#define CHARDELIM '\\'
#endif
#else
#ifdef VMS
#define CURRENTDELIM "[]"
#define FINALDELIM ""
#define CHARDELIM '.'
#else
#define CURRENTDELIM "./"
#define FINALDELIM "/"
#define CHARDELIM '/'
#endif
#endif

extern char * findFile(char *fileName);
extern char * getPWD(void);

#endif /* _file_h */
